<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT ;

$router->get('/list_game', function () {
    $results = app('db') ->select("SELECT * FROM Game");
    return response()->json($results);
});

$router->get('/list_allbuygame', function(Illuminate\Http\Request $request) {
    
    $results = app('db') ->select("SELECT * FROM buygame");
    return response()->json($results);
});

$router->get('/list_buygame', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

    $user = app('auth')->user();
    $userID = $user->id;

    $results = app('db') ->select("SELECT GameName, GamePrice, GameType, QuantityToBuy, ConfirmBuy 
                                   FROM Game, BuyGame, user
                                   WHERE (Game.GameID = BuyGame.GameID)
                                          AND (Game.GameID = ?)
                                          AND (user.UserID = BuyGame.UserID)",
                                          [$userID] );
    return response()->json($results);
}]);

$router->post('/addmin_add_game', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

    $user = app('auth')->user();
    $userID = $user->id;

    $results = app('db')->select("SELECT IsAdmin FROM USER WHERE UserID=?",
								  [$userID]);

    if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		
		$game_name = $request->input("game_name");

        $game_price = $request->input("game_price");

        $game_type = $request->input("game_type");

        $game_amount = $request->input("game_amount");
    
        $query = app('db')->insert('INSERT into Game
                                    (GameName, GamePrice, GameType, GameAmount)
                                    VALUES(?,?,?,?)',
                                    [ $game_name,
                                      $game_price,
                                      $game_type,
                                      $game_amount] );
    return "Ok";
	}
}]);

$router->put('/addmin_update_game', function(Illuminate\Http\Request $request) {

    $game_id = $request->input("game_id");

    $game_name = $request->input("game_name");

    $game_price = $request->input("game_price");

    $game_type = $request->input("game_type");

    $game_amount = $request->input("game_amount");
    
    $query = app('db')->update('UPDATE Game
                                    SET GameName=?,
                                        GamePrice=?,
                                        GameType=?,
                                        GameAmount=?
                                    WHERE
                                        GameID=?',
                                    [ $game_name,
                                      $game_price,
                                      $game_type,
                                      $game_amount,
                                      $game_id] );
    return "Ok";
});

$router->delete('/delete_game', function(Illuminate\Http\Request $request) {

    $game_id = $request->input("game_id");

    $query = app('db')->delete('DELETE FROM Game
                                    WHERE
                                        GameID=?',
                                        [ $game_id ] );

    return "Ok";
});

$router->post('/buy_game', ['middleware' => 'auth', function(Illuminate\Http\Request $request) {

    $game_id = $request->input("game_id");

    $buy_quantitytobuy = $request->input("buy_quantitytobuy");

    $user = app('auth')->user();
    $userID = $user->id;

    $query = app('db')->insert('INSERT into BuyGame 
                                    (GameID, UserID, QuantityToBuy, ConfirmBuy)
                                    VALUES (?, ?, ?, ?)',
                                     [ $game_id,
                                       $userID,
                                       $buy_quantitytobuy,
                                       'n'] );

    return "Ok";
}]);

$router->post('/register_user', function(Illuminate\Http\Request $request) {

    $name = $request->input("name");

    $surname = $request->input("surname");

    $email = $request->input("email");

    $username = $request->input("username");

    $password = app('hash')->make($request->input("password"));

    $query = app('db')->insert('INSERT into USER
                                    (Name, Surname, Email, Username, Password, IsAdmin)
                                    VALUES (?, ?, ?, ?, ?, ?)',
                                     [ $name,
                                       $surname,
                                       $email,
                                       $username,
                                       $password,
                                       'n', ] );

    return "Ok";
});

$router->post('/login', function(Illuminate\Http\Request $request) {

    $username = $request->input("username");
    $password = $request->input("password");

    $result = app('db')->select('SELECT UserID,password,IsAdmin from user WHERE Username=?',
                                [$username]);

    $loginResult = new stdClass();

    if(count($result) == 0) {
        $loginResult->status = "fail";
        $loginResult->reason = "User is not founded";
    }else {
        if(app('hash')->check($password, $result[0]->password)) {
            $loginResult->status = "success";

            $payload = [
                'iss' => "game_system",
                'sub' => $result[0]->UserID,
                'iat' => time(),
                'exp' => time() + 30 * 60 * 60,
            ];

            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            $loginResult->isAdmin = $result[0]->IsAdmin;

            return response()->json($loginResult);
        }else {
            $loginResult->status = "fail";
            $loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
        }
    }
    return response()->json($loginResult);
});

$router->put('/confirm_buy', ['middleware' => 'auth', function(Illuminate\Http\Request $request){

    $user = app('auth')->user();
    $userID = $user->id;

    $results = app('db')->select("SELECT IsAdmin FROM user WHERE UserID = ?", [$userID]);

    if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
        $reg_id = $request->input("reg_id");
		$query = app('db')->insert('UPDATE BuyGame
                                    SET confirmbuy="y"
                                    WHERE
                                        RegID=?',
                                        [ $reg_id ] );
        return "Ok";	
	}
}]);

$router->get('/get_user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$userID = $user->id;
	
	$results = app('db')->select('SELECT * FROM user
							      WHERE (user.UserID=?)',
										[$userID]);
	return response()->json($results);
}]);

$router->get('/admin_list_confirmbuy', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$userID = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM user WHERE UserID=?",
								  [$userID]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		
			$results = app('db')->select('SELECT RegID, Name, Surname, GameName, ConfirmBuy
												FROM game, buygame, user
												WHERE (buygame.GameID=game.GameID)
												AND (buygame.ConfirmBuy=?)
												AND  (user.UserID=buygame.UserID)',
												['n']);
		
		return response()->json($results);
	}
	
}]);