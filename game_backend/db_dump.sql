-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for game_system
CREATE DATABASE IF NOT EXISTS `game_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `game_system`;

-- Dumping structure for table game_system.buygame
CREATE TABLE IF NOT EXISTS `buygame` (
  `RegID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GameID` int(10) unsigned NOT NULL,
  `UserID` int(11) unsigned NOT NULL DEFAULT '0',
  `QuantityToBuy` int(11) NOT NULL,
  `ConfirmBuy` char(50) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`RegID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table game_system.buygame: ~3 rows (approximately)
/*!40000 ALTER TABLE `buygame` DISABLE KEYS */;
REPLACE INTO `buygame` (`RegID`, `GameID`, `UserID`, `QuantityToBuy`, `ConfirmBuy`) VALUES
	(1, 1, 0, 2, 'y'),
	(2, 1, 1, 3, 'y'),
	(3, 2, 2, 2, 'n');
/*!40000 ALTER TABLE `buygame` ENABLE KEYS */;

-- Dumping structure for table game_system.game
CREATE TABLE IF NOT EXISTS `game` (
  `GameID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GameName` varchar(50) NOT NULL DEFAULT '',
  `GamePrice` int(10) unsigned NOT NULL,
  `GameType` varchar(100) NOT NULL DEFAULT '',
  `GameAmount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`GameID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table game_system.game: ~3 rows (approximately)
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
REPLACE INTO `game` (`GameID`, `GameName`, `GamePrice`, `GameType`, `GameAmount`) VALUES
	(1, 'BallRun', 50, 'Survival', 5),
	(2, 'EarthShake', 100, 'Fighting', 6),
	(4, 'ppap', 10, 'pap', 10);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;

-- Dumping structure for table game_system.user
CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `IsAdmin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table game_system.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`UserID`, `Name`, `Surname`, `Email`, `Username`, `Password`, `IsAdmin`) VALUES
	(1, 'Suntipap', 'Supreeyathitikul', 'suntipap.s@gmail.com', 'tempura12345', '$2y$10$.RblXUV5MI742vEysJcPUuRY9Kvfkcvgz1jJGidCpme4MsAJZMo1C', 'n'),
	(2, 'Admin', 'Suntipap', 'suntipap.s@gmail.com', 'admin', '$2y$10$sAB0sNBPhca22MexhL4ZxOmpt8TpQj8F.3lDNRLMOPBC7EbYdp.wq', 'y'),
	(3, 'bbb', 'aaa', 'ss@ss', 'ma', '$2y$10$bmiptuCW7io3DwNrcprgouEN9OVvlT.HDW3fDvfxopdn23465dW5C', 'y');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
