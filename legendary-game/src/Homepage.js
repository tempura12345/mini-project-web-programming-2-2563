import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  function Homepage() {
      
      const [username, setUsername] = useState("");
      const [password, setPassword] = useState("");
      const [isLoggedInToMember, setIsLoggedInToMember] = useState(false);
      const [isLoggedInToAdmin,  setIsLoggedInToAdmin] =  useState(false);
      
      
      useEffect (()=>{

          if (sessionStorage.getItem('user_api_token')!=null) {
              setIsLoggedInToMember(false);
          }
          
          if (sessionStorage.getItem('admin_api_token')!=null) {
              setIsLoggedInToAdmin(false);
          }
      
      }, []);
      
      function sendLogin(){	
      
              axios.post('http://localhost/api/v1/login',
                  {
                      "username" : username,
                      "password" : password,
                  }
              ).then (
                  res=> {				
                      if (res.data.status=="success"){
		
                          if ( res.data.isAdmin == 'n'){
                              setIsLoggedInToMember(true);
                              sessionStorage.setItem('user_api_token', res.data.token);
                          }else{
                              setIsLoggedInToAdmin(true);
                              sessionStorage.setItem('admin_api_token', res.data.token);
                          }
                      }else {
                          alert ("ล็อคอินไม่สำเร็จ");
                      }
                  }
              );	
      }
      
      return (
          <div class="sp-container">
              <div class="sp-content">
				<div class="sp-globe"></div>
              
			  <h4>Legendary Game</h4>
			  <h3> Login </h3>
              <h3>Username: <input type="text" onChange={(e)=>{setUsername(e.target.value)}}/ ></h3>
              <h3>Password: <input type="password" onChange={(e)=>{setPassword(e.target.value)}}/ ></h3>
      

			  <button onClick={()=>{sendLogin()}} >Submit</button><br/><br/>
			  
              <Link to="/signup"> สมัครสมาชิก </Link>
              {isLoggedInToMember && <Redirect to="/member" /> }
              {isLoggedInToAdmin &&  <Redirect to="/admin" /> }
          </div>
		</div>
      );
  }
  
  export default Homepage;