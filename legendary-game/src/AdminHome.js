import './Decorate.css'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";
  
  import AdminAddGame from './AdminAddGame';
  import AdminConfirmOrder from './AdminConfirmOrder';
  import UpdateGame from './UpdateGame';
  import DeleteGame from './DeleteGame';
  
  
  
  import { useState, useEffect } from 'react';
  
  
  
  function AdminHome() {
      
      const [hasToken, setHasToken] = useState(true);	
  
      
      function logout() {
          
          sessionStorage.clear();
          setHasToken(false);
          alert("Logged Out");
      }
      return (<div>
      
              <h3>Welcome to Legendary Game database management system</h3>
			<img src="/picture/service.png" width="300"  height="250"/>
          <h3> <Link to="/admin/addgame"> AddGame </Link>   &nbsp;&nbsp;&nbsp;&nbsp;
               <Link to="/admin/confirm">  ConfirmOrder  </ Link>  &nbsp;&nbsp;&nbsp;&nbsp;
               <Link to="/admin/updategame">  UpdateGame </ Link>  &nbsp;&nbsp;&nbsp;&nbsp;
               <Link to="/admin/deletegame">  DeleteGame </ Link>  &nbsp;&nbsp;&nbsp;&nbsp;    
               
              
               <button onClick={ ()=>{logout()}}> Logout </button>
          </h3>
          {!hasToken && <Redirect to="/" /> }
          <hr />
          
              <Switch>
                  <Route path="/admin/addgame/">
                      <AdminAddGame />
                  </Route>
                  
                  
                  <Route path="/admin/confirm/">
                      <AdminConfirmOrder />
                  </Route>

                  <Route path="/admin/updategame/">
                      <UpdateGame />
                  </Route>

                  <Route path="/admin/deletegame/">
                      <DeleteGame />
                  </Route>
                 
                  
                  
              </Switch>
                  
          
      
      </div>)
  
  }
  export default AdminHome;