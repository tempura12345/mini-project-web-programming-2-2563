
import './App.css';
import  Homepage from './Homepage';
import  SignUp from './Signup';
import  MemberArea from './MemberArea';
import  AdminHome from './AdminHome';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
		<Router>
			<Switch>
				
				<Route exact path="/">
					<Homepage />
				 </Route>
				  
				 <Route path="/signup">
					<SignUp />
				</Route>

				<Route path="/member">
					<MemberArea />
				  </Route>
		  
		  
				<Route path="/admin">
					<AdminHome />
				
				</Route>
		  
			
			</Switch>
			
		</Router>
    </div>
  );
}

export default App;
