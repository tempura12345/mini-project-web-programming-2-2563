import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  
  
  function AdminAddGame() {
  
      const [hasToken, setHasToken] = useState(true);	
      
      
      const [gameName, 	setGameName] 	= useState("");
      const [gamePrice, 	setGamePrice] 	= useState("");
      const [gameType,  setGameType] 	= useState("");
	  const [gameAmount,  setGameAmount] 	= useState("");
      
      useEffect (()=>{
              
          if (sessionStorage.getItem('admin_api_token')==null) {
              setHasToken(false);
          }
      }, []);
      
      
      function sendAddGame(){
          axios.post('http://localhost/api/v1/addmin_add_game',
                  {
                      'api_token': sessionStorage.getItem('admin_api_token'),
                      'game_name' : gameName,
                      'game_price' : gamePrice,
                      'game_type' : gameType,
					  'game_amount' :gameAmount,
                      
                  }
              ).then (
                  res=> {	
                      
                      if (res.data == "Ok") {
                              alert("Success!!!");
                      }else {
                              alert ("Fail!!!");
                      }
                  }
              );
      }
      
      
      return (<div>
                  {!hasToken && <Redirect to="/" /> }
                  <h3> AddGame </h3>
                  
                  <h3> ชื่อเกม: <input type="text"  				value={gameName} 	onChange={(e)=>setGameName(e.target.value)}	/ > </h3>
                  <h3> ราคา: <input type="number" 				value={gamePrice} 	onChange={(e)=>setGamePrice(e.target.value)}	/ > </h3>
                  <h3> ประเภทเกม: <input type="text" 				value={gameType}  onChange={(e)=>setGameType(e.target.value)} 	/ > </h3>
                  <h3> จำนวนเกม: <input type="number" 				value={gameAmount}  onChange={(e)=>setGameAmount(e.target.value)} 	/ > </h3>

                  <button onClick={()=>{sendAddGame()}}>Send</button>
              
              </div>);
          
  }
  
  export default AdminAddGame;