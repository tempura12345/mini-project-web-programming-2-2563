import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  
  
  function DeleteGame(){
      
      
      const [hasToken, setHasToken] = useState(true);	
      const [gameList, setGameList] = useState([]);
      const [gameID, setGameID] = useState('');
  
      useEffect (()=>{
              
          if (sessionStorage.getItem('admin_api_token')==null) {
              setHasToken(false);
          }
      }, []);
      
      useEffect (()=>{
          
        if (sessionStorage.getItem('admin_api_token')==null) {
            setHasToken(false);
        }else {
            axios.get('http://localhost/api/v1/list_game',
            ).then (
                res=> {	
                        let item_list = [];
                        for(let i = 0;i < res.data.length;i++){
                            item_list[i] = [ res.data[i].GameID, res.data[i].GameName, res.data[i].GamePrice, res.data[i].GameType]; 
                        }
                        setGameList(item_list);
                }
            );	
         }
        }, []);

      function sendDeleteGame(){
          axios.put('http://localhost/api/v1/delete_game',
                  {
                      'api_token': sessionStorage.getItem('admin_api_token'),
                      'game_id' : gameID,
                      
                  }
              ).then (
                  res=> {	
                      
                      if (res.data == "Ok") {
                              alert("ลบสำเร็จ !!!");
                      }else {
                              alert ("fail !!!");
                      }
                  }
              );
      }
     
      
      
      return (
          <div>
              <h3> Delete </h3>
              
              
              {!hasToken && <Redirect to="/" /> }
              {gameList.map(eventItem => (
                  <h3> 
                      {eventItem[0]}: เกม:{eventItem[1]} ราคา:{eventItem[2]}  ประเภท: {eventItem[3]} 
                      <button onClick={()=>{  sendDeleteGame(eventItem[0]) } } > delete </button> 
                    
                  
                  </h3>
              ))}
              
              
              
          </div>
      
      );
  }
  
  export default DeleteGame;