import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  function AdminConfirmRegistration() {
      
      const [hasToken, setHasToken] = useState(true);	
      const [eventList, setEventList] = useState([]);
      
      
      function sendConfirm(regID){
          axios.put('http://localhost/api/v1/admin_confirm_user',
                  {
                      'api_token': sessionStorage.getItem('admin_api_token'),
                      'reg_id' : regID,
                  }
              ).then (
                  res=> {			
                      if (res.data == "Ok") {
                              alert("อนุมัติสมัคร สำเร็จ");
                              axios.get('http://localhost/api/v1/admin_list_register?api_token='+sessionStorage.getItem('admin_api_token'),
                              ).then (
                                  res=> {	
                                  
                                          
                                          let item_list = [];
                                          for(let i = 0;i < res.data.length;i++){
                                              item_list[i] = [ res.data[i].Name, res.data[i].Surname, res.data[i].EventName , res.data[i].RegID ]; 
                                          }
                                          
                                          setEventList(item_list);
                                          
                                  }
                              
                              );
                              
                      }else {
                              alert ("เกิดปัญหา สมัครไม่ได้");
                      }
                  }
              );
      }
      
      useEffect (()=>{
          
          console.log(sessionStorage.getItem('admin_api_token'));
          if (sessionStorage.getItem('admin_api_token')==null) {
              setHasToken(false);
          }else {
              axios.get('http://localhost/api/v1/admin_list_register?api_token='+sessionStorage.getItem('admin_api_token'),
              ).then (
                  res=> {	
                  
                          
                          let item_list = [];
                          for(let i = 0;i < res.data.length;i++){
                              item_list[i] = [ res.data[i].Name, res.data[i].Surname, res.data[i].EventName , res.data[i].RegID ]; 
                          }
                          
                          setEventList(item_list);
                          
                  }
              
              );
              
              
          
          }
      }, []);
      
      return (<div>
                  
                  <h2> อนุมัติการสมัคร </h2>
                  
                  {eventList.map(eventItem => (
                      <h4> - ชื่อ:{eventItem[0]} นามสกุล:{eventItem[1]}  วันเวลา: {eventItem[2]}  
                              <button onClick={()=>{  sendConfirm(eventItem[3]) } } >กดเพื่ออนุมัติการสมัคร</button> 
                      </h4>
                  ))}
                  
              </div>);
          
  }
  
  export default AdminConfirmRegistration;