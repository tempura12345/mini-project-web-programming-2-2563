import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  
  
  function Buygame(){
      
      
      const [hasToken, setHasToken] = useState(true);	
      const [gameList, setGameList] = useState([]);
      const [quantitytobuy, 	setQuantitytobuy] 	= useState("");

     
  
  
      useEffect (()=>{
              
          if (sessionStorage.getItem('user_api_token')==null) {
              setHasToken(false);
          }
      }, []);
      
      
      function sendSignup(gameID){
          axios.post('http://localhost/api/v1/buy_game',
                  {
                      'api_token': sessionStorage.getItem('user_api_token'),
                      'game_id' : gameID,
                      'buy_quantitytobuy' : quantitytobuy,
                      
                  }
              ).then (
                  res=> {	
                      
                      if (res.data == "Ok") {
                              alert("ซื้อสำเร็จ");
                      }else {
                              alert ("เกิดปัญหา ซื้อไม่ได้");
                      }
                  }
              );
      }
      useEffect (()=>{
          
          if (sessionStorage.getItem('user_api_token')==null) {
              setHasToken(false);
          }else {
              axios.get('http://localhost/api/v1/list_game',
              ).then (
                  res=> {	
                          let item_list = [];
                          for(let i = 0;i < res.data.length;i++){
                              item_list[i] = [ res.data[i].GameID, res.data[i].GameName, res.data[i].GamePrice, res.data[i].GameType]; 
                          }
                          setGameList(item_list);
                  }
              );	
          }
      }, []);
      
      
      return (
          <div>
              <h3> ซื้อเกม </h3>
              
              
              {!hasToken && <Redirect to="/" /> }
              {gameList.map(eventItem => (
                  <h3> 
                      {eventItem[0]}: เกม:{eventItem[1]} ราคา:{eventItem[2]}  ประเภท: {eventItem[3]}  	
                      
                    <button onClick={()=>{  sendSignup(eventItem[0]) } } > กดเพื่อซื้อ </button> 
                  
                  </h3>
              ))}
            <h3>  จำนวนเกมที่ต้องการซื้อ: <input type="number" value={quantitytobuy} 	onChange={(e)=>setQuantitytobuy(e.target.value)}/></h3>
              
          </div>
      
      );
  }
  
  export default Buygame;