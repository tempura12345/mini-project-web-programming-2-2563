import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  
  
  function UpdateGame() {
  
      const [hasToken, setHasToken] = useState(true);	
      
      const [gameID, 	setGameId] 	= useState("");
      const [gameName, 	setGameName] 	= useState("");
      const [gamePrice, 	setGamePrice] 	= useState("");
      const [gameType,  setGameType] 	= useState("");
	  const [gameAmount,  setGameAmount] 	= useState("");
      
      useEffect (()=>{
              
          if (sessionStorage.getItem('admin_api_token')==null) {
              setHasToken(false);
          }
      }, []);
      
      
      function sendUpdateGame(){
          axios.put('http://localhost/api/v1/addmin_update_game',
                  {
                      'api_token': sessionStorage.getItem('admin_api_token'),
                      'game_id' : gameID,
                      'game_name' : gameName,
                      'game_price' : gamePrice,
                      'game_type' : gameType,
					  'game_amount' :gameAmount,
                      
                  }
              ).then (
                  res=> {	
                      
                      if (res.data == "Ok") {
                              alert("Success!!!");
                      }else {
                              alert ("Fail!!!");
                      }
                  }
              );
      }
      
      
      return (<div>
                  {!hasToken && <Redirect to="/" /> }
                  <h3> UpdateGame </h3>
                  
                  <h3> GameID: <input type="number"  				value={gameID} 	onChange={(e)=>setGameId(e.target.value)}	/ > </h3>
                  <h3> ชื่อเกม: <input type="text"  				value={gameName} 	onChange={(e)=>setGameName(e.target.value)}	/ > </h3>
                  <h3> ราคา: <input type="number" 				value={gamePrice} 	onChange={(e)=>setGamePrice(e.target.value)}/ > </h3>
                  <h3> ประเภทเกม: <input type="text" 				value={gameType}  onChange={(e)=>setGameType(e.target.value)} 	/ > </h3>
                  <h3> จำนวนเกม: <input type="number" 				value={gameAmount}  onChange={(e)=>setGameAmount(e.target.value)} 	/ > </h3>

                  <button onClick={()=>{sendUpdateGame()}}>Send</button>
              
              </div>);
          
  }
  
  export default UpdateGame;