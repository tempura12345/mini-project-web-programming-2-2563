import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  function AdminConfirmOrder() {
      
      const [hasToken, setHasToken] = useState(true);	
      const [gameList, setGameList] = useState([]);
      
      
      function sendConfirm(regID){
          axios.put('http://localhost/api/v1/confirm_buy',
                  {
                      'api_token': sessionStorage.getItem('admin_api_token'),
                      'reg_id' : regID,
                  }
              ).then (
                  res=> {			
                      if (res.data == "Ok") {
                              alert("Success!!!");
                              axios.get('http://localhost/api/v1/admin_list_confirmbuy?api_token='+sessionStorage.getItem('admin_api_token'),
                              ).then (
                                  res=> {	
                                  
                                          
                                          let item_list = [];
                                          for(let i = 0;i < res.data.length;i++){
                                              item_list[i] = [ res.data[i].Name, res.data[i].Surname, res.data[i].GameName , res.data[i].RegID ]; 
                                          }
                                          
                                          setGameList(item_list);
                                          
                                  }
                              
                              );
                              
                      }else {
                              alert ("Fail!!!");
                      }
                  }
              );
      }
      
      useEffect (()=>{
          
          console.log(sessionStorage.getItem('admin_api_token'));
          if (sessionStorage.getItem('admin_api_token')==null) {
              setHasToken(false);
          }else {
              axios.get('http://localhost/api/v1/admin_list_confirmbuy?api_token='+sessionStorage.getItem('admin_api_token'),
              ).then (
                  res=> {	
                  
                          
                          let item_list = [];
                          for(let i = 0;i < res.data.length;i++){
                              item_list[i] = [ res.data[i].Name, res.data[i].Surname, res.data[i].GameName , res.data[i].RegID ]; 
                          }
                          
                          setGameList(item_list);
                          
                  }
              
              );
              
              
          
          }
      }, []);
      
      return (<div>
                  
                  <h3> CONFIRMBUY </h3>
                  
                  {gameList.map(eventItem => (
                      <h3> - Name:{eventItem[0]} Surname{eventItem[1]}  Game: {eventItem[2] }  
                              <button onClick={()=>{  sendConfirm(eventItem[3]) } } >Confirmbuy</button> 
                      </h3>
                  ))}
                  
              </div>);
          
  }
  
  export default AdminConfirmOrder;