import './Decorate.css'
import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  function ListMyEvent(){
      
      const [hasToken, setHasToken] = useState(true);	
      const [eventList, setEventList] = useState([]);
      
      useEffect (()=>{
          
          
          if (sessionStorage.getItem('user_api_token')==null) {
              setHasToken(false);
          }else {
              axios.get('http://localhost/api/v1/list_buygame?api_token='+sessionStorage.getItem('user_api_token'),
              ).then (
                  res=> {	
                          let item_list = [];
                          for(let i = 0;i < res.data.length;i++){
                              item_list[i] = [ res.data[i].GameName, res.data[i].GamePrice, res.data[i].GameType, res.data[i].ConfirmBuy ]; 
                          }
                          
                          setEventList(item_list);
                          
                  }
              
              );
              
              
          
          }
      }, []);
      
      
      return ( 
      <div>
      
          <h3>  เกมที่คุณซื้อ </h3>
      
           
           {eventList.map(eventItem => (
                  <h3> - เกม :{eventItem[0]} ราคา :{eventItem[1]}  ประเภท : {eventItem[2]} , สถานะยืนยันการซื้อ :{eventItem[3]} </h3>
            ))}
        
      </div>);
      
  }
  
  export default ListMyEvent;